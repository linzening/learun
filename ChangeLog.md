【20170603 index.html】
因为最后一个窗口关不掉，只能隐藏一个，隐藏那个为默认页，这样所有的窗口都能全部关闭啦，然后再隐藏掉默认显示的其他窗口，那几个无效。
	【更改后】
<a href="javascript:;" class="menuTab active" data-id="/Home/Default"></a>
	【原始】
<a href="javascript:;" class="menuTab active" data-id="/Home/Default">欢迎首页</a>



【20170603 js/index.js】
js/index.js第2024行的subchildNodesrow.F_UrlAddress改为subrow.F_UrlAddress，得以解决问题
	【更改后】
_html += '<li><a class="menuItem" data-id="' + subrow.F_ModuleId + '" href="' + subchildNodesrow.F_UrlAddress + '"><i class="' + subchildNodesrow.F_Icon + '"></i>' + subchildNodesrow.F_FullName + '</a></li>';

	【原始】
_html += '<li><a class="menuItem" data-id="' + subrow.F_ModuleId + '" href="' + subrow.F_UrlAddress + '"><i class="' + subchildNodesrow.F_Icon + '"></i>' + subchildNodesrow.F_FullName + '</a></li>';


【20180129 js/index.js】
新增js下面的两个文件：可以把菜单目录json分解成一个独立的json文件，这样方便直接修改json文件就可以了，或者可以修改json url从后台获取目录列表
新增文件：
index_json.js
index.json
如果需要使用这种方式，那么把index.js替换掉就行了。
